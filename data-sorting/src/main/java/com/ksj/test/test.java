package com.ksj.test;

import com.ksj.Utils.DataSorting.DataSortingTool;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.Listener;

import java.io.UnsupportedEncodingException;

public class test {

    public static void main(String[] args) {

        MqttTool.listenMgs("UPDATA", new Listener() {
            @Override
            public void onConnected() {

            }

            @Override
            public void onDisconnected() {

            }

            @Override
            public void onPublish(UTF8Buffer utf8Buffer, Buffer buffer, Runnable runnable) {

                try {
                    String content = new String(buffer.toByteArray(),"UTF8");
                    if (content.length() > 0) {
                        System.out.println(content);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }
}

package com.ksj;

import com.ksj.Config.RemoteConfigRefreshBlock;
import com.ksj.Config.RemoteConfigs;
import com.ksj.Service.MainService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import java.util.Map;


@SpringBootApplication
@EnableEurekaClient
public class DataSortingApplication {

    public static void main(String[] args)
    {
        /**注册基于配置的服务*/
        RemoteConfigs.registRefreshBlock(new RemoteConfigRefreshBlock() {
            @Override
            public void hasInit() {
                //初始化完成开启我们的服务
                MainService.startService();
            }

            @Override
            public void hasRefresh(Map<String, Object> changes) {
                System.out.println("********************" + changes);
                if (changes.containsKey("mysql.host")
                        || changes.containsKey("mysql.port")
                        || changes.containsKey("apollo.host")
                        || changes.containsKey("mysql.port")) {
                    //更新配置时先停止服务再开启新服务
                    MainService.restart();
                }
            }
        });

        SpringApplication.run(DataSortingApplication.class, args);
    }


}

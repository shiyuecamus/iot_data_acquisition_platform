package com.ksj.Config;

import com.ksj.Utils.SpringUtil.SpringBeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Configuration
@RefreshScope
public class RemoteConfigs {

    //mqsql ip
    @Value("${mysql.host}")
    public String mysql_host;

    //mqsql 端口号
    @Value("${mysql.port}")
    public String mysql_port;

    //mqsql 用户名
    @Value("${mysql.username}")
    public String mysql_username;

    //mqsql 密码
    @Value("${mysql.password}")
    public String mysql_password;

    //mqsqk 数据库名
    @Value("${mysql.database.name}")
    public String mysql_database_name;

    //mqsql 备用ip(通常为本机ip)
    @Value("${mysql.bhost}")
    public String mysql_bhost;

    //mqsql 备用端口(通常为本机ip端口)
    @Value("${mysql.bport}")
    public String mysql_bport;

    //apollo ip
    @Value("${apollo.host}")
    public String apollo_host;

    //apollo 端口
    @Value("${apollo.port}")
    public String apollo_prot;

    //apollo 用户名
    @Value("${apollo.username}")
    public String apollo_username;

    //apollo 密码
    @Value("${apollo.password}")
    public String apollo_password;

//    //apollo 备用ip
//    @Value("${apollo.bhost}")
//    public String apollo_bhost;
//
//    //apollo 备用端口
//    @Value("${apollo.bport}")
//    public String apollo_bport;

    //apollo 重连最大次数
    @Value("${apollo.reconnect.count}")
    public String apollo_reconnect_count;

    //apollo 重连间隔(单位：ms)
    @Value("${apollo.reconnect.dealy}")
    public String apollo_reconnect_dealy;

    //apollo 连接前清空会话
    @Value("${apollo.clean.start}")
    public String apollo_clean_start;

    //apollo 心跳频率(单位：s)
    @Value("${apollo.keep.alive}")
    public String apollo_keep_alive;

    //apollo 最大缓冲大小(单位:M)
    @Value("${apollo.buffer.size}")
    public String apollo_buffer_size;


    private static Logger LOG = LoggerFactory.getLogger(RemoteConfigs.class);
    public static RemoteConfigs instance;
    private static boolean isInit = true;
    private static RemoteConfigRefreshBlock _block;
    private static Map<String, Object> currentMap = new HashMap<>();

    public static RemoteConfigs getInstance() {
        if (instance == null) {
            instance = new RemoteConfigs();
        }
        return instance;
    }

    public static void registRefreshBlock(RemoteConfigRefreshBlock block) {
        _block = block;
    }

    @PostConstruct
    public void init() {
        instance = this;

        if (isInit) {
            if (_block != null)
                _block.hasInit();
            isInit = false;
            currentMap = SpringBeanUtils.getMainConfig();
            return;
        }
        if (_block != null)
            _block.hasRefresh(changes(currentMap, SpringBeanUtils.getMainConfig()));
        currentMap = SpringBeanUtils.getMainConfig();
    }

    private Map<String, Object> changes(Map<String, Object> before, Map<String, Object> after) {
        Map<String, Object> result = new HashMap();
        Iterator var4 = before.keySet().iterator();

        String key;
        while (var4.hasNext()) {
            key = (String) var4.next();
            if (!after.containsKey(key)) {
                result.put(key, (Object) null);
            } else if (!this.equal(before.get(key), after.get(key))) {
                result.put(key, after.get(key));
            }
        }

        var4 = after.keySet().iterator();

        while (var4.hasNext()) {
            key = (String) var4.next();
            if (!before.containsKey(key)) {
                result.put(key, after.get(key));
            }
        }

        return result;
    }

    private boolean equal(Object one, Object two) {
        if (one == null && two == null) {
            return true;
        } else {
            return one != null && two != null ? one.equals(two) : false;
        }
    }

    @PreDestroy
    public void over() {
        LOG.info("Spring上下文销毁了Bean~!");
        //注入bean
        SpringBeanUtils.getBean(this.getClass());
        LOG.info("Spring上下文加载了Bean~!");
    }

}



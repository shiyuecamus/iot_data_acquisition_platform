package com.ksj.Config;

import java.util.Map;

/**
 * block回调
 */
public interface RemoteConfigRefreshBlock {
    /**
     * 初始化回调
     */
    void hasInit();

    /**
     * 更新时回调
     */
    void hasRefresh(Map<String, Object> changes);
}

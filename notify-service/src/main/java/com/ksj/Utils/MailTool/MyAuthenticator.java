package com.ksj.Utils.MailTool;

import javax.mail.Authenticator;


/**
 密码验证器
 */
public class MyAuthenticator extends Authenticator{
    String userName=null;
    String password=null;

    private MyAuthenticator(){}
    public MyAuthenticator(String username, String password) {
        this.userName = username;
        this.password = password;
    }
    protected javax.mail.PasswordAuthentication getPasswordAuthentication(){
        return new javax.mail.PasswordAuthentication(userName, password);
    }
}

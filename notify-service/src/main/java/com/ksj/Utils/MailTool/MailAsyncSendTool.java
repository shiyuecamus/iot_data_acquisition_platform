package com.ksj.Utils.MailTool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MailAsyncSendTool implements Runnable {
    private String toAddress;
    private String subject;
    private String content;

    private static Logger LOG = LoggerFactory.getLogger(MailAsyncSendTool.class);

    private MailAsyncSendTool() {
    }

    public MailAsyncSendTool(String toAddress, String subject, String content) {
        this.toAddress = toAddress;
        this.subject = subject;
        this.content = content;
    }

    @Override
    public void run() {
       try {
           boolean fs = SimpleMailSender.sendTextMail(toAddress,
                   subject, content);
           if (fs) {
               LOG.info("发送邮件到" + toAddress + "成功!");
           } else {
               LOG.info("发送邮件到" + toAddress + "失败!");
           }
       }catch (Exception e){
          LOG.error("发送邮件失败！" + e.getMessage());
       }finally {

       }
    }
}

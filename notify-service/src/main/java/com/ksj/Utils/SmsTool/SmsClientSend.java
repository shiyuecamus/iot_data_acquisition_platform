package com.ksj.Utils.SmsTool;

/**
 * 
 */


import java.net.URLEncoder;


/**
 * @author zh
 *
 */
public class SmsClientSend {
	
	public static String url = "http://www.smswang.net:7803/sms";
	public static String account = "000147";
	public static String password = "123456qwe";
	public static String extno = "1069032239089147";
	private SmsClientSend() {}  
    private static SmsClientSend single=null;  
    //静�?工厂方法   
    public static SmsClientSend getInstance() {  
         if (single == null) {    
             single = new SmsClientSend();  
         }    
        return single;  
    }  
       
    public  String sms(String url, String account,
			String password, String mobile, String content,
			String extno) {

		try {
			account = URLEncoder.encode(account, "UTF-8");
			password = URLEncoder.encode(password, "UTF-8");
			StringBuffer send = new StringBuffer();
			send.append("action=send");
			send.append("&account=").append(account);
			send.append("&password=").append(password);
			send.append("&mobile=").append(mobile);
			send.append("&content=").append(URLEncoder.encode(content, "UTF-8"));
			send.append("&extno=").append(extno);
			return SmsClientAccessTool.getInstance().doAccessHTTPPost(url,
					send.toString(), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			return "未发送，编码异常";
		}
	}
    
    public String send(String mobile,String content) {
		String sendStr = single.sms(url, account, password, mobile, content, extno);
		System.out.println(sendStr);
		return sendStr;
	}
}

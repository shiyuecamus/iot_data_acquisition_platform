package com.ksj.Utils.SmsTool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SmsAsyncSendTool implements Runnable {
    private String phone;
    private String content;
    private static Logger LOG = LoggerFactory.getLogger(SmsAsyncSendTool.class);
    private SmsAsyncSendTool() { }

    public SmsAsyncSendTool(String phone, String content) {
        this.phone = phone;
        this.content = content;
    }

    @Override
    public void run() {
       try {
           String result = SmsClientSend.getInstance().send(phone,content);
           if (!result.equals("未发送，编码异常")){
               LOG.info("发送消息到" + phone + "成功~!");
           }else
               LOG.info(result);
       }catch (Exception e){
           LOG.error("发送短信失败！" + e.getMessage());
       }
    }
}

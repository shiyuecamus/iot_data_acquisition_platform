package com.ksj.Service;



import com.ksj.Utils.JdbcTool.JdbcTool;
import com.ksj.Utils.MqttTool.MqttTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MainService {

    private static Logger LOG = LoggerFactory.getLogger(MainService.class);

    public static void startService() {
        //初始化Mqtt
        MqttTool.init();
        //初始化JDBC
        JdbcTool.init();


        MqttTool.subscribeQueueMsg("ALARM/NOTIFY", new AlarmNotify());

    }

    //停止服务
    public static void stopService(){
        MqttTool.stop(false);
        JdbcTool.stop(false);
    }

    //重启服务
    public static void restart(){
        MqttTool.stop(true);
        JdbcTool.stop(true);
    }

}

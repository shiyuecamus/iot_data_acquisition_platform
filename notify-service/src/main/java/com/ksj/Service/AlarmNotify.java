package com.ksj.Service;


import com.ksj.Utils.JdbcTool.JdbcTool;
import com.ksj.Utils.MailTool.MailAsyncSendTool;
import com.ksj.Utils.SmsTool.SmsAsyncSendTool;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.List;
import java.util.Map;

public class AlarmNotify implements MessageListener {
    private static Logger LOG = LoggerFactory.getLogger(AlarmNotify.class);

    //收到队列消息
    public void onMessage(Message message) {
        try {
            //获取报警内容
            String data = ((TextMessage) message).getText();

            //检测报警字段
            checkAlarm(data);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //检测报警字段
    public void checkAlarm(String data) {
        //判断数据结构是List还是Map
        JSONObject jsonObj = new JSONObject(data);
        //获取数据的phone字段并查询数据库所对应的设备类型
        String phone = jsonObj.getString("PHONE");
        //获取body
        JSONObject body = jsonObj.getJSONObject("body");
        for (String key : body.keySet()) {
            //获取body中的每一个对象
            JSONObject object = body.getJSONObject(key);
            //获取对象中的REG_EMERG
            JSONObject REG_EMERG = object.getJSONObject("REG_EMERG");
            for (String emergKey : REG_EMERG.keySet()) {


                //发送推送消息
                String[] cols = {"info", "type"};
                List<Map<String, Object>> list = JdbcTool.select(cols, "alarm_notify", "phone = '" + phone + "' AND " + "point = '" + emergKey
                        + "' AND rank = '" + REG_EMERG.get(emergKey) + "'");
                for (Map<String, Object> map : list) {
                    if (map.get("info") == null
                            || map.get("info").toString().length() == 0
                            || map.get("type") == null
                            || map.get("type").toString().length() == 0) {
                        break;
                    }
                    Object subO = map.get("type");
                    Integer type = 0;
                    if (subO instanceof String) {
                        type = Integer.valueOf((String) subO);
                    } else if (subO instanceof Integer) {
                        type = (Integer) subO;
                    }
                    switch (type) {
                        case 1:
                            new Thread(new MailAsyncSendTool(String.valueOf(map.get("info")),
                                    "报警", emergKey + "点位报警了~!")).start();
                            break;
                        case 2:
                            //短信通知
                            Integer rank = Integer.valueOf(REG_EMERG.get(emergKey).toString());
                            String statu = rank == 1 ? "普通报警状态" : rank == 10 ? "严重报警状态" : rank == 100 ? "紧急报警状态" : "";
                            new Thread(new SmsAsyncSendTool(String.valueOf(map.get("info")),
                                    "AGV小车设备发生故障，故障原因:有障碍，请您尽快处理。【东浦信息】")).start();
                            break;
                        default:
                            break;
                    }
                }


            }
        }


    }
}

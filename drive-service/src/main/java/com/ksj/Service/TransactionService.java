package com.ksj.Service;

import com.ksj.Utils.JdbcTool.JdbcTool;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class TransactionService implements MessageListener {

    private static Logger LOG = LoggerFactory.getLogger(TransactionService.class);

    @Override
    public void onMessage(Message message) {
        try {
            String msg = ((TextMessage) message).getText();
            handleData(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //处理数据
    public static void handleData(String data) {

        /**回复消息 修改消息记录状态**/
        JSONObject obj = new JSONObject(data);
        String idx = String.valueOf(obj.get("IDX"));
        if (idx != null && idx.length() > 0) {
            String sql = "UPDATE jiaoyi SET status = 9999 WHERE IDX = '" + idx + "';";

            try {

                int result = JdbcTool.getSt().executeUpdate(sql);
                if (result > 0) {
                    LOG.info("修改回复交易成功~！");
                } else {
                    LOG.info("修改回复交易失败~！");
                }
            } catch (Exception e) {

                LOG.error("SQL异常！\n" + sql,e);
            }
        } else {
            LOG.info("没有找到回复消息的idx~！修改状态失败");
        }
    }
}
